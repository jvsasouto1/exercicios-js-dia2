// Criar array com números aleatórios
function criaArrays(n){
    array = []
    for (let i = 0; i < n; i++){
        array.push(Math.floor((Math.random()*100) + 1));
    }
    return array
}
// Ordenar array
function troca(listaNums, x, y){
    var temp = listaNums[x];
    listaNums[x] = listaNums[y];
    listaNums[y] = temp;
}
function ordenaArray(listaNums, tamanhoLista){
    var i;
    var j;
    for (let i = 0; i < tamanhoLista - 1; i++){
        for (let j = 0; j < tamanhoLista-i-1; j++){
            if(listaNums[j] > listaNums[j+1]){
                troca(listaNums, j, j+1);
            }
        }
    }
}

// Chamar funções
var qtdNumeros = 10;
var array = criaArrays(qtdNumeros);
console.log("Lista desordenada: "+ array);
ordenaArray(array, array.length);
console.log("Lista ordenada: "+ array);



