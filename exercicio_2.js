/*Implemente um algoritmo que pegue duas matrizes (array de arrays) e realize sua multiplicação. 
Lembrando que para realizar a multiplicação dessas matrizes o número de colunas da primeira matriz tem 
que ser igual ao número de linhas da segunda matriz. (2x2)
	Caso teste 1 : [ [ [2],[-1] ], [ [2],[0] ] ] e  [ [2,3],[-2,1] ] multiplicadas dão  [ [6,5], [4,6] ] 
	Caso teste 2 : [ [4,0], [-1,-1] ] e [ [-1,3], [2,7] ]  multiplicadas dão  [ [-4,12], [-1,-10] ] 
*/
// Obs.: 

function multiplicaMatriz(matrizUm, matrizDois){
    linhasMatrizUm = matrizUm.length
    linhasMatrizDois = matrizDois.length
    colunasMatrizUm = matrizUm[0].length
    colunasMatrizDois = matrizDois[0].length
    if (colunasMatrizUm != linhasMatrizDois){
        console.log("A matriz não pode ser multiplicada! O número de colunas da matriz 1 deve ser igual ao número de linhas da matriz 2")
    }
    let matrizFinal = new Array(linhasMatrizUm)
    for (let i = 0; i < matrizFinal.length; i++){
        matrizFinal[i] = new Array(colunasMatrizDois).fill(0);
    }
    for (x=0; x < matrizFinal.length; x++){
        for (y=0; y < matrizFinal[0].length; y++){
            for (z=0; z < colunasMatrizUm; z++){
                matrizFinal[x][y] += matrizUm[x][z] * matrizDois[z][y]
            }
        }
    }
    return matrizFinal
}

var matrizUm = [[4, 0], [-1, -1]]
var matrizDois = [[-1, 3], [2, 7]]
resultado = multiplicaMatriz(matrizUm, matrizDois)
console.log(resultado)
